export function getPercentOf(base, amount) {
  return (amount / base) * 100;
}

export function getAmountByPercent(base, percent) {
  return (percent / 100) * base;
}

export function roundToTwoDecimals(number) {
  return Number(number).toFixed(2);
}
