import moment from "moment";
import "moment/locale/es";

export const ES_LOCALE_1 = "D MMM YYYY";
export const ES_LOCALE_2 = "YYYY MM D";

export function getDateToSpanishString(date = new Date()) {
  const format = "D MMMM YYYY";

  return moment(date).format(format);
}

export function getDateToESLocale1(date) {
  return moment(date).format(ES_LOCALE_1);
}

export function getDateToEsLocale2(date) {
  return moment(date).format(ES_LOCALE_2);
}

export function parseDateToDayAndMonth(date) {
  return moment(date).format("D MMMM");
}

export function parseDateToDueMonthDay(date, monthDay) {
  const dateOfMonth = moment(date).date();

  //Parsing date to monthDay
  if (dateOfMonth > monthDay) {
    const leftDays = Math.abs(monthDay - dateOfMonth);

    date = moment(date).subtract(leftDays, "days");
  } else {
    const leftDays = Math.abs(dateOfMonth - monthDay);

    date = moment(date).add(leftDays, "days");
  }

  return date;
}

export function parseDateToLocalDateTime(date = new Date()) {
  return moment(date).format(moment.DATETIME_LOCAL_MS);
}

export function compareDates(dateA, dateB) {
  dateA = moment(dateA).format(ES_LOCALE_1);
  dateB = moment(dateB).format(ES_LOCALE_1);

  return moment(dateA).isSame(dateB);
}
