export function formatToRDCurrency(toFormat) {
  const formatter = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  });

  return "RD" + formatter.format(toFormat);
}

export function formatNumberToDecimal(number) {
  return parseFloat(number).toLocaleString("en", { minimumFractionDigits: 2, maximumFractionDigits: 2 });
}
