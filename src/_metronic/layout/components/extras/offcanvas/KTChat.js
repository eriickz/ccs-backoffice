import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_helpers";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

export function KTChat() {
  return (
    <OverlayTrigger placement="bottom" overlay={<Tooltip id="kt_chat_tooltip">Chat</Tooltip>}>
      <div className="topbar-item" data-toggle="tooltip" title="Quick panel" data-placement="right">
        <div className="btn btn-icon btn-clean btn-lg mr-1" id="kt_chat">
          <span className="svg-icon svg-icon-xl svg-icon-primary">
            <SVG src={toAbsoluteUrl("/media/svg/icons/Communication/Group-chat.svg")} />
          </span>
        </div>
      </div>
    </OverlayTrigger>
  );
}
