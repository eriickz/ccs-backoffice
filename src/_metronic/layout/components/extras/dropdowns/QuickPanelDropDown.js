import React from "react";
import SVG from "react-inlinesvg";
import { toAbsoluteUrl } from "../../../../_helpers";
import { OverlayTrigger, Tooltip } from "react-bootstrap";

export function QuickPanelDropDown() {
  return (
    <OverlayTrigger placement="bottom" overlay={<Tooltip id="quick-panel-tooltip">Quick panel</Tooltip>}>
      <div className="topbar-item" data-toggle="tooltip" title="Quick panel" data-placement="right">
        <div className="btn btn-icon btn-clean btn-lg mr-1" id="kt_quick_panel_toggle">
          <span className="svg-icon svg-icon-xl svg-icon-primary">
            <SVG src={toAbsoluteUrl("/media/svg/icons/Layout/Layout-4-blocks.svg")} />
          </span>
        </div>
      </div>
    </OverlayTrigger>
  );
}
