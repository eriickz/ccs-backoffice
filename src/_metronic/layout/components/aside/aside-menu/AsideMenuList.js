/* eslint-disable no-script-url,jsx-a11y/anchor-is-valid */
import React from "react";
import { useLocation } from "react-router";
import { NavLink } from "react-router-dom";
import { checkIsActive } from "../../../../_helpers";

export function AsideMenuList({ layoutProps }) {
  const location = useLocation();
  const getMenuItemActive = (url, hasSubmenu = false) => {
    return checkIsActive(location, url) ? ` ${!hasSubmenu && "menu-item-active"} menu-item-open ` : "";
  };

  return (
    <>
      {/* begin::Menu Nav */}
      <ul className={`menu-nav ${layoutProps.ulClasses}`}>
        <li className={`menu-item ${getMenuItemActive("/dashboard", false)}`} aria-haspopup="true">
          <NavLink className="menu-link" to="/dashboard">
            <span className="menu-text">
              <i className="icon" data-feather="grid" />
              Dashboard
            </span>
          </NavLink>
        </li>

        <li className={`menu-item ${getMenuItemActive("/consultar-ordenes", false)}`} aria-haspopup="true">
          <NavLink className="menu-link" to="/consultar-ordenes">
            <span className="menu-text">
              <i className="icon" data-feather="layers" />
              Órdenes
            </span>
          </NavLink>
        </li>

        <li className={`menu-item ${getMenuItemActive("/proveedores", false)}`} aria-haspopup="true">
          <NavLink className="menu-link" to="/proveedores">
            <span className="menu-text">
              <i className="icon" data-feather="users" />
              Proveedores
            </span>
          </NavLink>
        </li>

        <li className={`menu-item ${getMenuItemActive("/configuracion", false)}`} aria-haspopup="true">
          <NavLink className="menu-link" to="/configuracion">
            <span className="menu-text">
              <i className="icon" data-feather="settings" />
              Configuración
            </span>
          </NavLink>
        </li>
      </ul>
      {/* end::Menu Nav */}
    </>
  );
}
