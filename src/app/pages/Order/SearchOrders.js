import React, { useState, useEffect, useRef, useCallback } from "react";
import Select from "react-select";
import { loginUser, getOrders } from "./api";
import { Pagination } from "../../../_metronic/_partials/controls/pagination/Pagination";
import { formatNumberToDecimal } from "../../../_metronic/_helpers";
import moment from "moment";

export default function SearchOrders() {
  const [orders, setOrders] = useState([]);
  const [suppliers, setSuppliers] = useState([]);
  const [airlines, setAirlines] = useState([]);
  const [filtersItems, setFiltersItems] = useState([]);
  const [selectedFilter, setSelectedFilter] = useState(undefined);
  const [selectedFilterItem, setSelectedFilterItem] = useState("");

  const intervalId = useRef(null);
  const isAirlinesSetted = useRef(false);

  const filters = [
    {
      value: 0,
      label: "Aerolineas",
    },
    {
      value: 1,
      label: "Proveedores",
    },
  ];

  const paginationProps = {
    totalSize: 11,
    sizePerPage: 10,
    page: 1,
    paginationSize: 1,
    sizePerPageList: [50, 75, 100, 125, 150],
  };

  const processOrders = useCallback(() => {
    getOrders().then(({ data }) => {
      let allOrders = [];
      const allAirlines = [];

      if (!isAirlinesSetted.current) {
        data.aerolineas.map(aerolinea => {
          allAirlines.push({
            value: aerolinea.name,
            label: aerolinea.name,
          });

          return aerolinea;
        });
      }

      data.result.map(order => {
        let { aerolinea, flight_departure, amount_total, flight_number, delivered, estado, seq_numero, fecha_creacion, moneda, _orderitems_ } = order;

        //Date formats
        const hora_salida = moment.utc(flight_departure).format("hh:mma");

        console.log(hora_salida);

        fecha_creacion = moment(fecha_creacion).format("DD/MM/YYYY");

        //Getting supplier name
        const proveedor = _orderitems_[0].proveedor.nombre;
        const proveedor_id = _orderitems_[0].proveedor.id;

        //Getting currency
        const { moneda_simbolo } = moneda;

        //Getting order detail
        const orden_detalle = _orderitems_[0]._productos_.nombre;

        //Processing order label
        let estatus_detalle = estado.toLowerCase();

        if (estatus_detalle === "en proceso") {
          estatus_detalle = "en-proceso";
        }

        allOrders.push({
          amount_total,
          flight_number,
          delivered,
          estado,
          hora_salida,
          fecha_creacion,
          proveedor,
          moneda_simbolo,
          orden_detalle,
          seq_numero,
          aerolinea,
          estatus_detalle,
          proveedor_id,
        });

        return order;
      });

      if (selectedFilterItem !== "") {
        if (selectedFilter === 1) {
          allOrders = allOrders.filter(order => order.proveedor_id === selectedFilterItem);
        } else if (selectedFilter === 0) {
          allOrders = allOrders.filter(order => order.aerolinea === selectedFilterItem);
        }
      }

      setOrders(allOrders);

      if (!isAirlinesSetted.current) {
        setAirlines(allAirlines);
      }
    });
  }, [isAirlinesSetted, selectedFilter, selectedFilterItem]);

  const processSuppliers = () => {
    loginUser().then(({ data }) => {
      const allSuppliers = [];

      data.result.proveedores.map(prov => {
        if (prov.estado === "activo") {
          allSuppliers.push({
            label: prov.nombre,
            value: prov._id,
          });
        }

        return prov;
      });

      setSuppliers(allSuppliers);
    });
  };

  useEffect(() => {
    processOrders();
    processSuppliers();

    intervalId.current = setInterval(() => {
      processOrders();
    }, 5000);

    return () => clearInterval(intervalId.current);

    //eslint-disable-next-line
  }, [processOrders]);

  useEffect(() => {
    if (selectedFilter !== undefined) {
      switch (selectedFilter) {
        case 0:
          setFiltersItems([...airlines]);
          return;

        case 1:
          setFiltersItems([...suppliers]);
          return;

        default:
          break;
      }
    }

    //eslint-disable-next-line
  }, [selectedFilter]);

  const setDropdownValue = (valueId, options) => {
    const valueFound = options.find(opt => opt.value === valueId);
    return valueFound !== undefined ? valueFound : [];
  };

  return (
    <>
      <div className="content-header">
        <h5>Órdenes</h5>
      </div>
      <div className="card student-content">
        <div className="card-body">
          <div className="row">
            <div className="col-md-4 col-sm-12">
              <div className="form-group">
                <label>Filtrar Por:</label>
                <Select value={setDropdownValue(selectedFilter, filters)} options={filters} onChange={opt => setSelectedFilter(opt.value)} />
              </div>
            </div>
            <div className="col-md-4 col-sm-12">
              <div className="form-group">
                <label>Filtrar Por:</label>
                <Select value={setDropdownValue(selectedFilterItem, filtersItems)} options={filtersItems} onChange={opt => setSelectedFilterItem(opt.value)} />
              </div>
            </div>
            <div className="col-md-2 col-sm-4">
              <button className="btn btn-unibe bg-blue mt-13 ml-3">Buscar</button>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12">
              <table className="table-custom">
                <thead>
                  <tr>
                    <th>No. Orden</th>
                    <th>Vuelo</th>
                    <th>Hora de Salida</th>
                    <th>Proveedor</th>
                    <th>Detalle</th>
                    <th>Total</th>
                    <th>Fecha</th>
                    <th className="text-center">Estatus</th>
                  </tr>
                </thead>
                <tbody>
                  {orders.map(order => (
                    <tr>
                      <td>{order.seq_numero}</td>
                      <td>
                        {order.aerolinea} - {order.flight_number}
                      </td>
                      <td>{order.hora_salida}</td>
                      <td>{order.proveedor}</td>
                      <td>{order.orden_detalle}</td>
                      <td>
                        {order.moneda_simbolo}
                        {formatNumberToDecimal(order.amount_total)}
                      </td>
                      <td>{order.fecha_creacion}</td>
                      <td className="text-center">
                        <span className={`label label-rounded ${order.estatus_detalle}`}>{order.estado}</span>
                      </td>
                    </tr>
                  ))}
                </tbody>
              </table>
              <Pagination paginationProps={paginationProps} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
