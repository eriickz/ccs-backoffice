import React from "react";
import ReactDatatable from "@ashvin27/react-datatable";
import { config as tableConfig } from "./tableConfig";
import useSuppliersRetriever from "./hooks/useSuppliersRetriever";
import SupplierForm from "./Modals/SupplierForm";

export default function SuppliersList() {
  const { suppliers, columns, selectedSupplierId, setFetchSuppliers } = useSuppliersRetriever();

  return (
    <>
      <SupplierForm supplierId={selectedSupplierId} setFetchSuppliers={setFetchSuppliers} />
      <div className="content-header">
        <h5>Proveedores</h5>
      </div>
      <div className="card student-content">
        <div className="card-body">
          <div className="row">
            <div className="col-md-12">
              <ReactDatatable className="table-custom" config={tableConfig} columns={columns} records={suppliers} />
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
