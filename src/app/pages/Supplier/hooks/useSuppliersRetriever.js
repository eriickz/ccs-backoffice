import React, { useState, useEffect } from "react";
import { getSuppliers } from "../api";
import { suppliersColumns } from "../tableConfig";
import * as $ from "jquery";

const useSuppliersRetriever = () => {
  const [suppliers, setSuppliers] = useState([]);
  const [columns, setColumns] = useState(suppliersColumns);
  const [fetchSuppliers, setFetchSuppliers] = useState(true);
  const [selectedSupplierId, setSelectedSupplierId] = useState(undefined);

  useEffect(() => {
    if (fetchSuppliers) {
      getSuppliers().then(({ data }) => {
        const allColumns = [...columns];

        allColumns.push({
          key: "_id",
          text: "Acciones",
          sortable: false,
          cell: ({ _id }) => (
            <div className="button-group">
              <button className="material-icons" data-toggle="modal" data-target="#supplierFormModal" onClick={() => setSelectedSupplierId(_id)}>
                create
              </button>
              <button className="material-icons">delete</button>
            </div>
          ),
        });

        setSuppliers(data.result);
        setColumns(allColumns);
        setFetchSuppliers(false);
      });
    }
    //eslint-disable-next-line
  }, [fetchSuppliers]);

  useEffect(() => {
    if (selectedSupplierId !== undefined) {
      $("#supplierFormModal").modal("show");
    }
  }, [selectedSupplierId]);

  return { suppliers, columns, selectedSupplierId, setFetchSuppliers };
};

export default useSuppliersRetriever;
