import React from "react";
import Select from "react-select";
import InputMask from "react-input-mask";
import { useDropzone } from "react-dropzone";

export default function SupplierForm() {
  const { getRootProps, getInputProps, isDragActive, acceptedFiles } = useDropzone({
    maxFiles: 1,
    maxSize: 5000000,
  });

  return (
    <div class="modal fade custom-modal" id="supplierFormModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
      <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
          <div class="modal-body">
            <div className="row">
              <h3 className="unibe-subheader text-uppercase mb-5">Editar Proveedor</h3>
            </div>
            <div className="row">
              <div className="col-md-4">
                <div className="form-group">
                  <label>Código</label>
                  <input id="codigo" type="text" className="form-control" />
                </div>
              </div>
              <div className="col-md-4">
                <div className="form-group">
                  <label>Nombre</label>
                  <input id="nombre" type="text" className="form-control" />
                </div>
              </div>
              <div className="col-md-4">
                <div className="form-group">
                  <label>Nombre Singular</label>
                  <input id="nombre_singular" type="number" className="form-control" />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-4">
                <div className="form-group">
                  <label>RNC</label>
                  <input id="rnc" type="number" className="form-control" />
                </div>
              </div>
              <div className="col-md-4">
                <div className="form-group">
                  <label>Contacto</label>
                  <InputMask id="telefono_contacto" className="form-control" mask="99/99/9999" placeholder="dd/mm/yyyy" />
                </div>
              </div>
              <div className="col-md-4">
                <div className="form-group">
                  <label>Estado</label>
                  <Select />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="form-group">
                  <label>Descripción</label>
                  <textarea id="descripcion" className="form-control" />
                </div>
              </div>
            </div>
            <div className="row">
              <div className="col-md-12">
                <div className="form-group">
                  <label>Imagén del contenido</label>
                  <div {...getRootProps({ className: "dropzone form-control" })}>
                    <input {...getInputProps()} />
                    {isDragActive ? <p>Suelta los archivos aquí...</p> : <p>Suelta los archivos aquí, o presiona para seleccionar</p>}
                    <ul className="list-group mt-2">
                      {acceptedFiles.map(file => (
                        <li className="list-group-item">{file.name}</li>
                      ))}
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer pt-0">
            <button class="btn btn-unibe-clear" data-dismiss="modal" aria-label="Cerrar">
              Cancelar
            </button>
            <button class="btn btn-unibe" data-dismiss="modal">
              Modificar
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
