import React from "react";
import moment from "moment";

export const suppliersColumns = [
  {
    key: "fecha_creacion",
    text: "Creado El",
    sortable: true,
    cell: ({ fecha_creacion }) => moment(fecha_creacion).format("DD/MM/YYYY"),
  },
  {
    key: "codigo",
    text: "Código",
    sortable: true,
  },
  {
    key: "nombre",
    text: "Nombre",
    sortable: true,
  },
  {
    key: "descripcion",
    text: "Descripción",
    sortable: true,
  },
  {
    key: "rnc",
    text: "RNC",
    sortable: true,
  },
  {
    key: "telefono_contacto",
    text: "Contacto",
    sortable: true,
  },
  {
    key: "estado",
    text: "Estado",
    cell: ({ estado }) => {
      const labelType = estado ? "completado" : "pendiente";
      const cellText = estado ? "Activo" : "Inactivo";

      return <span className={`label label-rounded label-sm ${labelType}`}>{cellText}</span>;
    },
  },
];
