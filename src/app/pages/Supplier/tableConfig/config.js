export const config = {
  page_size: 10,
  length_menu: [10, 20, 50],
  language: {
    length_menu: "Mostrar _MENU_ registros por página",
    filter: "Buscar en registros...",
    info: "Mostrando _START_ de _END_ de _TOTAL_ registros",
    no_data_text: "No se han encontrado registros",
    pagination: {
      first: "Primero",
      previous: "Previo",
      next: "Próximo",
      last: "Último",
    },
  },
};
