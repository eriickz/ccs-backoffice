import React, { Suspense } from "react";
import { Redirect, Switch } from "react-router-dom";
import { LayoutSplashScreen, ContentRoute } from "../_metronic/layout";

import SearchOrders from "./pages/Order/SearchOrders";
import SuppliersList from "./pages/Supplier/SuppliersList";

export default function BasePage() {
  return (
    <Suspense fallback={<LayoutSplashScreen />}>
      <Switch>
        {
          /* Redirect from root URL to /dashboard. */
          <Redirect exact from="/" to="/consultar-ordenes" />
        }

        {/* Dashboard */}
        <ContentRoute path="/consultar-ordenes" component={SearchOrders} />
        <ContentRoute path="/proveedores" component={SuppliersList} />

        <Redirect to="/consultar-ordenes" />
      </Switch>
    </Suspense>
  );
}
