export default function setupAxios(axios, store) {
  axios.defaults.baseURL = "http://192.168.10.243:8000";

  axios.interceptors.request.use(
    config => {
      const {
        auth: { authToken },
      } = store.getState();

      if (authToken) {
        config.headers.Authorization = `Bearer ${authToken}`;
      }

      return config;
    },
    err => Promise.reject(err),
  );
}
