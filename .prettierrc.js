module.exports = {
  printWidth: 190,
  bracketSpacing: true,
  jsxBracketSameLine: true,
  singleQuote: false,
  trailingComma: "all",
  semi: true,
  useTabs: false,
  arrowParens: "avoid",
};
